#include "../rc-switch/RCSwitch.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

 
int main(int argc, char *argv[]) {
    int PIN = 0; // siehe wiring Pi Belegung
    // code-Family : 01100 da invertire in 10011
                                         // 1 0 0 1 1 i tasti    of on
    unsigned long codeFunksteckdoseAon  = 4277585; //0100000101 0001010101 0001
    unsigned long codeFunksteckdoseAoff = 4277588; //0100000101 0001010101 0100
    unsigned long codeFunksteckdoseBon  = 4280657; //0100000101 0100010101 0001
    unsigned long codeFunksteckdoseBoff = 4280660; //0100000101 0100010101 0100
    unsigned long codeFunksteckdoseCon  = 4281425; //0100000101 0101000101 0001
    unsigned long codeFunksteckdoseCoff = 4281428; //0100000101 0101000101 0100
    unsigned long codeFunksteckdoseDon  = 4281617; //0100000101 0101010001 0001
    unsigned long codeFunksteckdoseDoff = 4281620; //0100000101 0101010001 0100
 
    if (wiringPiSetup() == -1) return 1;
 
    RCSwitch mySwitch = RCSwitch();
    mySwitch.enableTransmit(PIN);
    mySwitch.setRepeatTransmit(4); // default = 10
 
    
    if (argc==1) {
        // se non ci sono parametri, non fare nulla
        
    } else if (atoi(argv[1]) > 99) {
        // esempi
        // ./433 4277585
        // ./433 4277588 4280660 4281428 4281620 
        for (int i=1; i < argc ; i++) {
            mySwitch.send(atoi(argv[i]), 24);
        }                
        
    } else if (argv[1][0] == 'R') {
        // esempi
        // ./433 R
        // ./433 R H
        // ./433 R M
        // ./433 R S
        // ./433 R H 21       R H <hour>
        // ./433 R M 36       R M <min>
        // ./433 R S 15       R S <sec>
        // ./433 R 15         R <sec>
        // ./433 R 15 36      R <sec> <min>
        // ./433 R 15 36 21   R <sec> <min> <sec>
        if (argc==2) {
            // ./433 R
            // se non ci sono ulteriori parametri, 
            // allora prendo l'orologio del computer    
            // e imposto secondi, minuti e ora, giorno, mese e anno
            time_t Zeitstempel = time(0);
            tm *adesso = localtime(&Zeitstempel);
    
            unsigned long CodiceDaInviare =  int(argv[1][0])<<16;
            
            mySwitch.send(CodiceDaInviare + (int('S')<<8) +  adesso->tm_sec, 24);
            mySwitch.send(CodiceDaInviare + (int('M')<<8) +  adesso->tm_min, 24);
            mySwitch.send(CodiceDaInviare + (int('H')<<8) +  adesso->tm_hour, 24);
            mySwitch.send(CodiceDaInviare + (int('w')<<8) +  adesso->tm_wday , 24);
            mySwitch.send(CodiceDaInviare + (int('d')<<8) +  adesso->tm_mday , 24);
            mySwitch.send(CodiceDaInviare + (int('m')<<8) +  adesso->tm_mon + 1, 24);
            mySwitch.send(CodiceDaInviare + (int('y')<<8) +  adesso->tm_year - 100, 24);
            
        } else if ((argc==3)
                    & (argv[2][0] == 'H' | argv[2][0] == 'M' | argv[2][0] == 'S'
                        | argv[2][0] == 'd')) { 
            // ./433 R H
            // ./433 R M
            // ./433 R S
            // ./433 R d
            // se non ci sono ulteriori parametri, 
            // allora prendo l'orologio del computer    
            time_t Zeitstempel = time(0);
            tm *adesso = localtime(&Zeitstempel);
    
            unsigned long CodiceDaInviare =  (int(argv[1][0])<<16)   + (int(argv[2][0])<<8);
            if (argv[2][0] == 'H') { CodiceDaInviare = CodiceDaInviare +  adesso->tm_hour ; }
            else if (argv[2][0] == 'M') { CodiceDaInviare = CodiceDaInviare + adesso->tm_min; }
            else if (argv[2][0] == 'S') { CodiceDaInviare = CodiceDaInviare + adesso->tm_sec; }
            else if (argv[2][0] == 'w') { CodiceDaInviare = CodiceDaInviare + adesso->tm_wday ; }
            else if (argv[2][0] == 'd') { CodiceDaInviare = CodiceDaInviare + adesso->tm_mday ; }
            else if (argv[2][0] == 'm') { CodiceDaInviare = CodiceDaInviare + adesso->tm_mon + 1 ; }
            else if (argv[2][0] == 'y') { CodiceDaInviare = CodiceDaInviare + adesso->tm_year - 100 ; }
            
            mySwitch.send(CodiceDaInviare, 24);
            
        } else if ((argc>3) & (argv[2][0] == 'H' | argv[2][0] == 'M' | argv[2][0] == 'S')) { 
            unsigned long CodiceDaInviare;
            CodiceDaInviare =  int(argv[1][0])<<16  ;
            if ( ( argv[2][0] == 'H' &  atoi(argv[3])<24 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'M' &  atoi(argv[3])<60 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'S' &  atoi(argv[3])<60 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'w' &  atoi(argv[3])< 8 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'd' &  atoi(argv[3])<32 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'm' &  atoi(argv[3])<13 & atoi(argv[3])>=0 )
                |( argv[2][0] == 'y' &  atoi(argv[3])<22 & atoi(argv[3])>=17 )
            ) { 
                // ./433 R H 21       R H <hour>
                // ./433 R M 36       R M <min>
                // ./433 R S 15       R S <sec>
                // ./433 R w  5       R w <day of month, 1..7, 1=mon>
                // ./433 R d 15       R d <day of month, 1..31>
                // ./433 R m  5       R w <month, 1..12, 1=jan>
                // ./433 R y 17       R w <year, 16... 
                CodiceDaInviare = CodiceDaInviare + (int(argv[2][0])<<8);
                
            } else {
                CodiceDaInviare = 0; 
            }
            CodiceDaInviare = CodiceDaInviare +  atoi(argv[3]);
            if (CodiceDaInviare != atoi(argv[3])) {
                mySwitch.send(CodiceDaInviare, 24);
            }
        } else if ((argc>2) &  atoi(argv[2])<60 & atoi(argv[2])>=0) {
            mySwitch.send( (int(argv[1][0])<<16) + (int('S')<<8) +  atoi(argv[2]) , 24);  
            
            if ((argc>3) & atoi(argv[3])<60 & atoi(argv[3])>=0) {
                mySwitch.send( (int(argv[1][0])<<16) + (int('M')<<8) +  atoi(argv[3]) , 24); 
                 
                if ((argc>4) & atoi(argv[4])<24 & atoi(argv[4])>=0) {
                    mySwitch.send( (int(argv[1][0])<<16) + (int('H')<<8) +  atoi(argv[4]) , 24);  
                }
            }
        }
        
    } else if (argc==3) {
        // esempi
        // ./433 1 0    corrisponde ad A off
        if ((atoi(argv[1]) == 1 | argv[1][0]=='A' | argv[1][0]=='a') & atoi(argv[2]) == 1) {mySwitch.send(codeFunksteckdoseAon, 24);}
        else if ((atoi(argv[1]) == 1 | argv[1][0]=='A' | argv[1][0]=='a') & atoi(argv[2]) == 0) {mySwitch.send(codeFunksteckdoseAoff, 24);}
        else if ((atoi(argv[1]) == 2 | argv[1][0]=='B' | argv[1][0]=='b') & atoi(argv[2]) == 1) {mySwitch.send(codeFunksteckdoseBon, 24);}
        else if ((atoi(argv[1]) == 2 | argv[1][0]=='B' | argv[1][0]=='b') & atoi(argv[2]) == 0) {mySwitch.send(codeFunksteckdoseBoff, 24);}
        else if ((atoi(argv[1]) == 3 | argv[1][0]=='C' | argv[1][0]=='c') & atoi(argv[2]) == 1) {mySwitch.send(codeFunksteckdoseCon, 24);}
        else if ((atoi(argv[1]) == 3 | argv[1][0]=='C' | argv[1][0]=='c') & atoi(argv[2]) == 0) {mySwitch.send(codeFunksteckdoseCoff, 24);}
        else if ((atoi(argv[1]) == 4 | argv[1][0]=='D' | argv[1][0]=='d') & atoi(argv[2]) == 1) {mySwitch.send(codeFunksteckdoseDon, 24);}
        else if ((atoi(argv[1]) == 4 | argv[1][0]=='D' | argv[1][0]=='d') & atoi(argv[2]) == 0) {mySwitch.send(codeFunksteckdoseDoff, 24);}
    }
    
    return 1;
}
