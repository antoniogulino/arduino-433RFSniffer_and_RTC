Funzione
========
Ascolta la porta seriale (USB) leggendo i numeri che gli passa Arduino
a mano a mano che Arduino sente segnali radio 433.

Scrive quanto ascoltato in un file mensile (`~/sensors/SerialUSB0-YYYYMM.tsv`).

Nel caso senta dei ben precisi segnali, allora fa anche cose extra
* *campanello senza fili Aukey*: allora attiva il beep di Pioneer600
* *PIR 433*: allora manda un telegramma (vedasi `tg-send-msg`)