/*
  Simple example for receiving
  
  https://github.com/sui77/rc-switch/
*/



///////////////////////////////
#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();



void setup() {
  Serial.begin(9600);
  mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2

  
}

unsigned long value;
void loop() {
  if (mySwitch.available()) {
    value = mySwitch.getReceivedValue();
    
    if (value != 0) {
        Serial.print( value );
        Serial.print("\n");
    }

    mySwitch.resetAvailable();
  }
}
// 0100 0011 1010 1010 1001 1001 = 4434585 = PIR
// 0100 0001 0101 0001 0101 0001 = 4280657 = telecomando: 01100 B ON
// 0100 0001 0101 0001 0101 0100 = 4280660 = telecomando: 01100 B OFF
// 0101 0101 0101 1101 0101 0101 = 5594453 = Sensore porta: Switch A6 = H
// 0101 0101 0101 0111 0101 0101 = 5592917 = Sensore porta: Switch A7 = H 
// 0101 0101 0101 0100 0101 0101 = 5592149 = Sensore porta: Switch A7 = L 
// 0111 1111 0100 0011 0001 1000 = 8340248 = campanello senza fili Aukey


