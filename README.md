Software
========
Software richiesto
------------------
# Arduino
* `RCSwitch.h` - installare rc-switch-master.zip scaricato da https://github.com/sui77/rc-switch


Hardware
========
Hardware richiesto
------------------
* Arduino Uno o Arduino Nano o altro di simile. Se si vuole usare `readSerialUSB0.py`, allora è meglio evitare Nano v3.0 (dicono che v.3.3 funzioni). Pare che Arduino Micro vada pure bene (ma non ho testato)
* ricevitore 433 Mhz
* RTC (DS1302) (non necessario se si vuole usare soltanto `readSerialUSB0.py`)

* Raspberry Pi
* emettitore 433 Mhz

Collegamenti:
------------
* RTC  CE pin    -> Arduino Digital 8
* RTC  I/O pin   -> Arduino Digital 7
* RTC  SCLK pin  -> Arduino Digital 6
* RTC  GND pin   -> Arduino Digital 5
* RTC  VCC pin   -> Arduino Digital 4
* 433Mhz receiver DATA -> Arduino Digital 2
* 433Mhz receiver GND  -> Arduino GND
* 433Mhz receiver VCC  -> Arduino 5V
* 433Mhz sender DATA -> Raspberry
* 433Mhz sender GND  -> Raspberry GND
* 433Mhz sender VCC  -> Raspberry 5V

Software
========
Arduino
-------
* `_433RFSniffer_and_RTC.ino`:  Legge i segnali 433 e li decodifica indicando l'ora del segnale
* `_433RFSniffer2Serial.ino `: Legge i segnali 433 e li decodifica inviando il risultato alla porta seriale

Raspberry Pi
------------
* `433.php` : Interfaccia web al programma 433
* `433` : Utilizza un emettitore 433 per inviare segnali radio codificati
* `readSerialUSB0.py`: legge i numeri provenienti dalla porta usb e scrive l'evento in un file

Utilizzo
========
Inviare un semplice codice (o più di uno)
-----------------------------------------
* `433 <codice>` 
* `433 <codice> <codice> <codice>` ecc. ecc.

Inviare segnali precodificati
-----------------------------
* `433 b 1` ... equivale a schiacciare "ON" del bottone "B"
* `433 C 0` ... equivale a schiacciare "OFF" del bottone "C"
* `433 R` ... invia ora esatta e giorno attuali
* `433 R 15` ... invia secondi=15
* `433 R 15 3` ... invia contemporaneamente secondi=15 e minuti=3
* `433 R 15 3 21` ... invia contemporaneamente secondi=15, minuti=3 e ora=21
* `433 R S` ... invia secondi attuali
* `433 R S 15` ... invia secondi=15
* `433 R M` ... invia minuti attuali
* `433 R M 3` ... invia minuti=3
* `433 R H` ... invia ora attuale
* `433 R w` ... invia giorno della settimana (1..7, 1=lunedi)
* `433 R d` ... invia giorno del mese (1..31)
* `433 R m` ... invia mese (1=gennaio)
* `433 R y` ... invia ultime due cifre dell'anno (17 in poi)

Come modificare l'ora o il giorno:
----------------------------------
Inviare tramite la frequenza 433 un segnale di 3 Byte composto nel seguente modo:
+ il secondo Byte rappresenta la lettera maiuscola "R" (come RTC o oRa o houR) 
+ il terzo Byte indica quale valore cambiare ("H", "M", "S", "w", "d", "m", "y")
+ il terzo Byte contiene il nuovo valore 

Pertanto per modificare
* l'ora: inviare il codice 537395 + 18432 + \<ora> = 5392384 + \<ora>
* minuti: inviare il codice 537395 + 19712 + \<minuti> = 5393664 + \<minuti>
* secondi: inviare il codice 537395 + 21248 + \<secondi> = 5395200 + \<secondi>
* giorno della settimana (1..7 , dove 1=lunedí):  inviare il codice 537395 + 21248 + \<wday> = 5395200 + \<wday>
* giorno del mese (1..31):  inviare il codice 537395 + 21248 + \<mday> = 5395200 + \<mday>
* mese (1..12): inviare il codice 537395 + 21248 + \<month> = 5395200 + \<month>
* anno (17..): inviare il codice 537395 + 21248 + \<year> = 5395200 + \<year>

Per impostare (p.es. con crontab) l'orario conviene aspettare che sia cambiata l'ora
e poi subito
* farsi dare e inviare prima il codice dei secondi, 
* successivamente farsi dare e inviare  il codice dei minuti 
* e infine farsi dare e inviare  il codice delle ore 

 
