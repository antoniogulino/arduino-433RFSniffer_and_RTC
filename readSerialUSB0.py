#!/usr/bin/python
# -*- coding: utf-8 -*-
import serial
import os
import time
from datetime import datetime
import csv

time_beep_prec = datetime.now();
time_PIR433_prec = datetime.now();

#ser = serial.Serial('/dev/ttyUSB0',9600)
ser = serial.Serial('/dev/ttyACM0',9600)
while True:
    riga = ser.readline()

    lt = time.localtime()
    jahr, monat, tag, stunde, minute, sekunde = lt[0:6]
    
    # do per scontato che arrivano solo numeri
    # questo deve essere garantito dallo sketch
    # sull'arduino
    numero = long(riga)
    if (numero == 4434585):      #        00000000 0100 0011 1010 1010 1001 1001 
        rigaOutput = "PIR 433"
        tempoPassatoUltimoPIR433 = datetime.now() - time_PIR433_prec
        if (tempoPassatoUltimoPIR433.total_seconds() > 3*60): # aspetta almeno 3 minuti tra un messaggio e l'altro

            destinatario = "@AntonioGulino"

            messaggio = "%d/%d/%d %d:%02d:%02d :" % (tag,monat,jahr, stunde,minute,sekunde)
            messaggio = messaggio + "PIR433: Qualcuno davanti la porta?"
            messaggio = messaggio + " Verifica con system tail -n 40 /home/pi/sensors/SerialUSB0-20*.tsv|/home/pi/bin/unique.pl"

            os.system("/home/pi/bin/tg-send-msg "+destinatario+" '"+messaggio+"' &")

            time_PIR433_prec = datetime.now()
 
    elif ((numero & 4289374890 == 0) & ((numero % 32 == 20) | (numero % 32 == 17 ))) :
        # per il telecomando Brennenstuhl e compatibili
        # i primi 8 segnali sono "0"
        # ogni coppia di segnali comincia con lo "0"
        # 4289374890 = 11111111101010101010101010101010

        # il segnale � "on" (17=_1_0_1 finale) o "off" (20=_1_1_0 finale)
        onoff = 'ON';
        if ((numero % 32) == 20):
            onoff = 'OFF'

        posizioneSwitch = ''
        posizioneSwitch = posizioneSwitch + str(1 - ((numero  & ((1<<22)))>>22))
        posizioneSwitch = posizioneSwitch + str(1 - ((numero  & ((1<<20)))>>20))
        posizioneSwitch = posizioneSwitch + str(1 - ((numero  & ((1<<18)))>>18))
        posizioneSwitch = posizioneSwitch + str(1 - ((numero  & ((1<<16)))>>16))
        posizioneSwitch = posizioneSwitch + str(1 - ((numero  & ((1<<14)))>>14))
       
        binarioCanale = (numero>>6) & 255 
        if (binarioCanale == ((0<<6) + (1<<4) + (1<<2) + 1)):  # 00010101 0111 X--- 21
            canale = 'A'
        elif (binarioCanale == ((1<<6) + (0<<4) + (1<<2) + 1)):# 01000101 1011 -X-- 69
            canale = 'B'
        elif (binarioCanale == ((1<<6) + (1<<4) + (0<<2) + 1)):# 01010001 1101 --X- 81 
            canale = 'C'
        elif (binarioCanale == ((1<<6) + (1<<4) + (1<<2) + 0)):# 01010100 1110 ---X 84
            canale = 'D'
        else:
            canale = '{0:08b}'.format(binarioCanale)
        #if (canale == 'A')  | (canale == 'B') | (canale == 'C')  | (canale=='D') :
        #    tempoPassatoUltimoBeep = datetime.now() - time_beep_prec
	#    if (tempoPassatoUltimoBeep.total_seconds()*1000 > 150):
	#	os.system("/home/pi/bin/beep &")
	#	time_beep_prec = datetime.now()
         
        rigaOutput= "telecomando: "+posizioneSwitch+" "+canale+" "+onoff

    
                                 #                 S1S2 S3S4 S5              OFON
    elif (numero == 4281695):    #        00000000 0100 0001 01 01 0101 0101 1111  
        rigaOutput = "telecomando: 01100 01010101 01 1111"
    elif (numero == 4277599):    #        00000000 0100 0001 01 00 0101 0101 1111  
        rigaOutput = "telecomando: 01100 A 01 1111"
    elif (numero == 4280671):    #        00000000 0100 0001 01 01 0001 0101 1111  
        rigaOutput = "telecomando: 01100 B 01 1111"
    elif (numero == 4281439):    #        00000000 0100 0001 01 01 0100 0101 1111  
        rigaOutput = "telecomando: 01100 C 01 1111"
    elif (numero == 4281429):    #        00000000 0100 0001 01 01 0100 0101 0101  
        rigaOutput = "telecomando: 01100 C 01 0101"
    elif (numero == 4281631):    #        00000000 0100 0001 01 01 0101 0001 1111  
        rigaOutput = "telecomando: 01100 D 01 1111"
    elif (numero == 4281623):    #        00000000 0100 0001 01 01 0101 0001 0111  
        rigaOutput = "telecomando: 01100 D 01 0111"


    elif (numero == 8340248):    #        00000000 0111 1111 0100 0011 0001 1000
        rigaOutput = "campanello senza fili Aukey"
        tempoPassatoUltimoBeep = datetime.now() - time_beep_prec
        if (tempoPassatoUltimoBeep.total_seconds()*1000 > 150):
            os.system("/home/pi/bin/beep &")
            time_beep_prec = datetime.now()
    elif (numero == 5592149):    #        00000000 0101 0101 0101 0100 0101 0101
        rigaOutput = "Sensore porta: Switch A7 = L"

                                 #                                          OFON
    elif (numero == 1135923):  #        00000000 0001 0001 0101 0101 0011 0011   0000 0101 11 11 03 03
        rigaOutput = "kwmobile: 0305 1 ON"
    elif (numero == 1135932):  #        00000000 0001 0001 0101 0101 0011 1100   0000 0101 11 11 03 30
        rigaOutput = "kwmobile: 0305 1 OFF"
    elif (numero == 1136067):  #        00000000 0001 0001 0101 0101 1100 0011   0000 0101 11 11 30 03
        rigaOutput = "kwmobile: 0305 2 ON"
    elif (numero == 1136076):  #        00000000 0001 0001 0101 0101 1100 1100   0000 0101 11 11 30 30
        rigaOutput = "kwmobile: 0305 2 OFF"
    elif (numero == 1136387):  #        00000000 0001 0001 0101 0111 0000 0011   0000 0101 11 13 00 03
        rigaOutput = "kwmobile: 0305 3 ON"
    elif (numero == 1136396):  #        00000000 0001 0001 0101 0111 0000 1100   0000 0101 11 13 00 30
        rigaOutput = "kwmobile: 0305 3 OFF"

    else:
        rigaOutput = numero

    filename = "/home/pi/sensors" + "/SerialUSB0-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"
    with open(filename, "a") as tsv:
        tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='"')
        tw.writerow([tag, monat, jahr, stunde, minute, sekunde, rigaOutput])
    #print(rigaOutput);
