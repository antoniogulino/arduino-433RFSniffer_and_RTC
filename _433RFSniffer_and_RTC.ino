/*
  Simple example for receiving
  
  https://github.com/sui77/rc-switch/
*/

// DS1302_Serial_Easy (C)2010 Henning Karlsen
// web: http://www.henningkarlsen.com/electronics
//
// Adopted for DS1302RTC library by Timur Maksimov 2014
//
// A quick demo of how to use my DS1302-library to 
// quickly send time and date information over a serial link
//
// I assume you know how to connect the DS1302.
// DS1302:  CE pin    -> Arduino Digital 8
//          I/O pin   -> Arduino Digital 7
//          SCLK pin  -> Arduino Digital 6
//          GND pin   -> Arduino Digital 5
//          VCC pin   -> Arduino Digital 4

///////////////////////////////
#include <Time.h>
#include <DS1302RTC.h>

// RTC Set pins:  CE, IO,CLK
DS1302RTC RTC(8, 7, 6);

// Optional connection for RTC module
#define DS1302_GND_PIN 5
#define DS1302_VCC_PIN 4

///////////////////////////////
#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();

// per sincronizzare l'orologio
char idAggiornamentoOra = 'R'; //  82, viene spostato di 16 bit => 5373952
char idGiornoSettimana  = 'w'; // 119, viene spostato di 8 bit => 30464 (+5373952 = 5404416)
char idAnno             = 'y'; // 121, viene spostato di 8 bit => 30976 (+5373952 = 5404928)
char idMese             = 'm'; // 109, viene spostato di 8 bit => 27904 (+5373952 = 5401856)
char idGiorno           = 'd'; // 100, viene spostato di 8 bit => 25600 (+5373952 = 5399552)
char idOra              = 'H'; // 72, viene spostato di 8 bit => 18432 (+5373952 = 5392384)
char idMinuti           = 'M'; // 77, viene spostato di 8 bit => 19712 (+5373952 = 5393664)
char idSecondi          = 'S'; // 83, viene spostato di 8 bit => 21248 (+5373952 = 5395200)
// Esempio
// impostare (R) i minuti (M) a 2 (2)
// 5373952 + 19712 + 2 =  5393666
// per esempio http://raspi34/433.php?code=5393666
// per impostare a zero i secondi http://raspi34/433.php?code=5395200

unsigned long istantePrec;
unsigned long codicePrec;


void print2digits(int number) {
  if (number >= 0 && number < 10)    Serial.write('0');
  Serial.print(number);
}

void setup() {
  Serial.begin(115200);
  mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2
  // Activate RTC module
  digitalWrite(DS1302_GND_PIN, LOW);
  pinMode(DS1302_GND_PIN, OUTPUT);

  digitalWrite(DS1302_VCC_PIN, HIGH);
  pinMode(DS1302_VCC_PIN, OUTPUT);
  
  Serial.println("RTC module activated");
  Serial.println();
  delay(500);
  
  if (RTC.haltRTC()) {
    Serial.println("The DS1302 is stopped.  Please run the SetTime");
    Serial.println("example to initialize the time and begin running.");
    Serial.println();
  }
  if (!RTC.writeEN()) {
    Serial.println("The DS1302 is write protected. This normal.");
    Serial.println();
  }
        
  
if (false) {  // per settare l'ora
 tmElements_t tm;
 RTC.read(tm);
 tm.Hour = 23;
 tm.Minute = 40;
 tm.Second = 5;

 tm.Wday = 7;

 tm.Day = 26;
 tm.Month = 3;
 tm.Year = CalendarYrToTm(2017);
 
  RTC.write(tm);
}  
  
}

void loop() {
  if (mySwitch.available()) {
    //Serial.println((0L+idSecondi)<<8);
    
    unsigned long value = mySwitch.getReceivedValue();
    
    if (value == 0) {
      Serial.print("Unknown encoding");
    } else {
        tmElements_t tm;

      unsigned long adesso=millis();
      if (adesso > istantePrec+1000 | value != codicePrec) {
        if (false) {
          Serial.print(adesso / 60000L);
          Serial.print("' ");
          Serial.print((adesso -(adesso / 60000L)*60000L)/1000L);
          Serial.print("\"");
        }
//  Serial.print("UNIX Time: ");
//  Serial.print(RTC.get());
        
     if (! RTC.read(tm)) {
    //    Serial.print(", DoW = ");
        if (tm.Wday == 1) {
          Serial.print("Mon");
        } else if (tm.Wday == 2) {
          Serial.print("Tue");
        } else if (tm.Wday == 3) {
          Serial.print("Wed");
        } else if (tm.Wday == 4) {
          Serial.print("Thu");
        } else if (tm.Wday == 5) {
          Serial.print("Fri");
        } else if (tm.Wday == 6) {
          Serial.print("Sat");
        } else if (tm.Wday == 7) {
          Serial.print("Sun");
        } else {
          Serial.print(tm.Wday);          
        }
        Serial.print(", ");
    //    Serial.print(", Date (D/M/Y) = ");
        Serial.print(tm.Day);
        Serial.print('.');
        Serial.print(tm.Month);
        Serial.print('.');
        Serial.print(tmYearToCalendar(tm.Year));
        //Serial.print("  Time = ");
        Serial.print(" ");
        print2digits(tm.Hour);
        Serial.write(':');
        print2digits(tm.Minute);
        Serial.write(':');
        print2digits(tm.Second);
      }         
        Serial.print(" : Code ");
  //      Serial.print( mySwitch.getReceivedValue() );       
        Serial.print( value );
        Serial.print(" / ");
        Serial.print( mySwitch.getReceivedBitlength() );
        Serial.print("bit ");
        Serial.print("Protocol: ");
        Serial.print( mySwitch.getReceivedProtocol() );
        if ((value>>16) == idAggiornamentoOra) {
          char b1 = value>>16;
          char b2 = (value <<16)>>24;
          byte b3 = (value <<24)>>24;
          Serial.print(" Aggiornamento RTC: ");
          //Serial.print(b1);
          //Serial.print(" ");
          //Serial.print(b2);
          //Serial.print(" ");
          //Serial.print(b3);
          //Serial.print(" ");
          if ((b2 == idGiornoSettimana) && (b3 > 0) && (b3 < 8)) {
            Serial.print("wday = ");
            Serial.print( b3 );
            tm.Wday = b3;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ((b2 == idAnno) && (b3 >16) && (b3 < 20)) {
            Serial.print("year = ");
            Serial.print( b3 );
            tm.Year = CalendarYrToTm(2000 +b3);
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ((b2 == idMese) && (b3 > 0) && (b3 < 13)) {
            Serial.print("month = ");
            Serial.print( b3 );
            tm.Month = b3 ;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ((b2 == idGiorno) && (b3 > 0) && (b3 < 32)) {
            Serial.print("day = ");
            Serial.print( b3 );
            tm.Day = b3;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ((b2 == idOra) && (b3 < 24)) {
            Serial.print("hh = ");
            Serial.print( b3 );
            tm.Hour = b3;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ( ((value>>8) == ((0L + idAggiornamentoOra)<<8) + idMinuti)
               && (b3 < 60)) { 
            Serial.print(" min = ");
            Serial.print( b3 );
            tm.Minute = b3;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          if ( ((value>>8) == ((0L + idAggiornamentoOra)<<8) + idSecondi)
               && (b3 < 60)) { 
            Serial.print(" sec = ");
            Serial.print( b3 );
            tm.Second = b3;
            RTC.write(tm);
            //Serial.print(" (syntax OK)");
          }
          Serial.println("");
        }
        if (value == 4277585) {Serial.print(" telecomando: 01100 A ON");};
        if (value == 4277588) {Serial.print(" telecomando: 01100 A OFF");};
        if (value == 4280657) {Serial.print(" telecomando: 01100 B ON");};
        if (value == 4280660) {Serial.print(" telecomando: 01100 B OFF");};
        if (value == 4281425) {Serial.print(" telecomando: 01100 C ON");};
        if (value == 4281428) {Serial.print(" telecomando: 01100 C OFF");};
        if (value == 4281617) {Serial.print(" telecomando: 01100 D ON");};
        if (value == 4281620) {Serial.print(" telecomando: 01100 D OFF");};
        if (value == 4281695) {Serial.print(" telecomando: 01100 coda ?");};
        if (value == 8340248) {Serial.print(" campanello senza fili Aukey");};        
        if (value == 4434585) {Serial.print(" PIR 433");};        
        Serial.println("");
        istantePrec = adesso;
        codicePrec = value;
      }
    }

    mySwitch.resetAvailable();
  }
}
// 0100 0011 1010 1010 1001 1001 = 4434585 = PIR
// 0100 0001 0101 0001 0101 0001 = 4280657 = telecomando: 01100 B ON
// 0100 0001 0101 0001 0101 0100 = 4280660 = telecomando: 01100 B OFF
// 0101 0101 0101 1101 0101 0101 = 5594453 = Sensore porta: Switch A6 = H
// 0101 0101 0101 0111 0101 0101 = 5592917 = Sensore porta: Switch A7 = H 
// 0101 0101 0101 0100 0101 0101 = 5592149 = Sensore porta: Switch A7 = L 
// 0111 1111 0100 0011 0001 1000 = 8340248 = campanello senza fili Aukey

